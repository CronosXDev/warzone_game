﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test_Firing : MonoBehaviour {

	public Transform hBulletInitPoint;
	public Transform hLookTarget;
	Vector3 vAimDirection;

	public float fFireRate;
	float fLastTimeFired = 0;


	Vector3 vRot;
	float fRotX;
	float fRotY;
	float fRotz;
	public float fOffset;
	public float fMultiplyRotX;
	public float fMultiplyRotY;


	//MAGAZINE
	public int nTotalAmmo;
	public int nMaxAmmoInMagazine;
	public int nCurrentAmmoinMagazine;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		updateAimDirection ();
//		UpdateRotation ();
		UpdateLookTarget();
		UpdateMagazineInput ();
		Fire (Time.time);

	}

	void updateAimDirection()
	{
		vAimDirection = hBulletInitPoint.transform.forward * 5;
//		vAimDirection = Camera.main.ScreenPointToRay (Input.mousePosition);
		Debug.DrawRay (hBulletInitPoint.position, vAimDirection);
//		Camera.main.world
	}

	void UpdateLookTarget()
	{
		fRotX = Input.GetAxis ("Mouse X");
		fRotY = Input.GetAxis ("Mouse Y");
		print (Vector3.Distance (this.transform.position, hLookTarget.transform.position));
		fRotz = Vector3.Distance (this.transform.position, hLookTarget.transform.position);

		if (fRotz < fOffset)
			fRotz += Time.deltaTime;
		else
			fRotz = 0;
		
		vRot = new Vector3 (fRotX  , fRotY , 0 );

		this.transform.Rotate (new Vector3 (0, fRotX * fMultiplyRotX , fRotY));
		hLookTarget.Translate (vRot);

		this.transform.LookAt (hLookTarget.position);
	}

	void UpdateRotation()
	{
		
//			transform.rotation = ;
		fRotX = Input.GetAxis ("Mouse X") * fMultiplyRotX;
		fRotY = Input.GetAxis ("Mouse Y") * -1 * fMultiplyRotY;

		vRot = new Vector3 (fRotY+fRotX , fRotY  ,  fRotX );
		transform.Rotate (vRot);
	}

	void UpdateMagazineInput()
	{
		if (Input.GetKeyDown (KeyCode.R)) {
			
			if (nTotalAmmo > 0) {
				if (nTotalAmmo > nMaxAmmoInMagazine) {
					nCurrentAmmoinMagazine = nMaxAmmoInMagazine;
					nTotalAmmo -= nMaxAmmoInMagazine;
				} else {
					nCurrentAmmoinMagazine = nTotalAmmo;
					nTotalAmmo = 0;
				}
			}


		}
	}

	bool isFire()
	{	
		if (Input.GetMouseButton (0)) {
			
			return true;

		}
		else
			return false;
	}



	void Fire(float fCurrentTime)
	{
		if (isFire ()) 
		{
			if (  fCurrentTime- fLastTimeFired > fFireRate) {

				print ("Fire");
				ManageMagazine ();
				fLastTimeFired = fCurrentTime;
			}
		}
	}


	void ManageMagazine()
	{
		if (nCurrentAmmoinMagazine > 0) {
			nCurrentAmmoinMagazine--;
		}
	}


	void OnGUI()
	{
		GUILayout.TextArea  (  nCurrentAmmoinMagazine + "  |  " + nTotalAmmo);
	}
}
