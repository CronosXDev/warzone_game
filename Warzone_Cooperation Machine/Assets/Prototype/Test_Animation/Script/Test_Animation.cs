﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test_Animation : MonoBehaviour {

	public float fVertButton;
	public float fHoriButton;
	public Animator hAnimator;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		fVertButton = Input.GetAxis ("Vertical");
		fHoriButton = Input.GetAxis ("Horizontal");

		hAnimator.SetFloat ("VerticalButton", fVertButton);
		hAnimator.SetFloat ("HorizontalButton", fHoriButton);
	}
}
