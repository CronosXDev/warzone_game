﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSwichStance : MonoBehaviour {

	public GameObject m_hMechFormModel;
	public GameObject m_hWeaponFormModel;
	int m_nCurrentForm;

	void Start()
	{
		m_nCurrentForm = (int)MECHFORM.mechform;
		SetForm ((int)MECHFORM.mechform);
	}
		

	void Update()
	{
		InputController ();
	}

	void InputController()
	{
		if (Input.GetKeyDown (KeyCode.F)) {
			//Switch Form
			if (m_nCurrentForm == (int)MECHFORM.mechform)
				m_nCurrentForm = (int)MECHFORM.weaponform;
			else
				m_nCurrentForm = (int)MECHFORM.mechform;

			SetForm (m_nCurrentForm);
		}
	}

	void SwitchForm()
	{
		if (m_nCurrentForm == (int)MECHFORM.mechform) {
			//Switch to Weapon Form
			SetForm((int)MECHFORM.mechform);
		} else {
			//Switch to Mech Form;
			SetForm((int)MECHFORM.weaponform);
		}
	}

	void SetForm(int nFormNumber)
	{
		if (nFormNumber == (int)MECHFORM.mechform) 
		{
			m_hWeaponFormModel.SetActive (false);
			m_hMechFormModel.SetActive (true);
		} 
		else if (nFormNumber == (int)MECHFORM.weaponform) 
		{
			m_hWeaponFormModel.SetActive (true);
			m_hMechFormModel.SetActive (false);
		}
	}


	public enum MECHFORM
	{
		mechform = 0,
		weaponform,
		count,
	}
}
