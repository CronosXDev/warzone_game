﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CBaseMechCharacter : MonoBehaviour {


	[Header("Mech Controller")]
	public CBaseMechController m_hMechController;
	[Header("Attribute")]
	public MECH_ATTRIBUTE m_hAttribute;
	[Header("Animator")]
	public Animator m_hAnimator;
	[Header("Eqioped Weapon")]
	public int m_nCurrentWeapon;
	[Header("Weapon Holster")]
	public CBaseWeapon[] m_arrWeaponHolster;
	public Vector3[] m_arrWeaponMointPointPosition;
	public Vector3[] m_arrWeaponMointPointRotation;
	[Header("Conclusion Attribute")]
	public float m_fMoveSpeed;


	public void AttachWeaponNumber(CBaseMechCharacter hOwner,CBaseWeapon hWeapon)
	{
		print ("Collect Weapon");

		hOwner.m_arrWeaponHolster [hWeapon.m_nWeaponType] = hWeapon;
		hWeapon.transform.parent = hOwner.gameObject.transform;
		Vector3 vOffset = m_arrWeaponMointPointPosition [hWeapon.m_nWeaponType];
//		hWeapon.transform.position = vOffset;
		hWeapon.transform.position = new Vector3 
			(vOffset.x + hWeapon.transform.position.x, 
			vOffset.y + hWeapon.transform.position.y,
			vOffset.z + hWeapon.transform.position.z);

		Vector3 vRotation = m_arrWeaponMointPointRotation [hWeapon.m_nWeaponType];
		hWeapon.transform.rotation = Quaternion.Euler( vRotation) * hOwner.transform.rotation; 
//		hWeapon.transform.rotation = Quaternion.Euler (hWeapon.transform.rotation.x + vRotation.x,
//			hWeapon.transform.rotation.y + vRotation.z,
//			hWeapon.transform.rotation.z + vRotation.z);
	}

}
