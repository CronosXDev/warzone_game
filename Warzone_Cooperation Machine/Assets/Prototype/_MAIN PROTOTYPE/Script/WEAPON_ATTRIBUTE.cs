﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WEAPON_ATTRIBUTE {

	public float m_fFirerate;
	public int m_nTotalAmmo;
	public int m_nMagazineAmmo;
	public bool m_bAutomatic;
}
