﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class CAMERA_STRUCTURE {

	public Camera m_hCamera;
	public GameObject m_hLookObject;
	public float m_fDistance;
	public float m_fMouseXSpeed = 1;
	public float m_fMouseYSpeed = 1;
}
