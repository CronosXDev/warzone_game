﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CBaseMechController : MonoBehaviour {
	
	[Header("Interaction")]
	public float m_fInteractSphereRadius;
	public float m_fInteractSphereDistance;
	public float m_fInteractSphereHeight;

	public enum ACTION
	{
		look = 0,
		aim,
		fire,
		move,
		crouch,
		jump,
		sprint,
		Interact,
		equipweapon,
		count,

	}

	public void ProcessInput(CBaseMechCharacter hOwner )
	{
		// Process input and return ActionNumber

	}

	public void TriggerInteractArea(CBaseMechCharacter hOwner)
	{
		Collider[] col = Physics.OverlapSphere (GetInteractSpherePosition(hOwner , 5), m_fInteractSphereRadius);

		for(int i= 0 ; i < col.Length ; i++){
			print ("TRIGGER INTERACT Weapon -- " + col.Length);
			if (col [i].gameObject.layer == LayerMask.NameToLayer ("weapon")) {


			hOwner.AttachWeaponNumber (hOwner, col [i].GetComponent<CBaseWeapon> ());
			}
		}

	}

	Vector3 vSpherePosition;
	public Vector3 GetInteractSpherePosition(CBaseMechCharacter hOwner,float fDistance)
	{
//		vSpherePosition = new Vector3 (vOffset.x,
//			vOffset.y,
//			vOffset.z);
		vSpherePosition =  hOwner.transform.position + hOwner.transform.forward * m_fInteractSphereDistance;
		vSpherePosition.y = hOwner.transform.position.y + m_fInteractSphereHeight;
//		Debug.DrawRay (hOwner.transform.position, hOwner.transform.forward  * 5, Color.red , 2);
		return vSpherePosition  ;// + vMechRotation;
	}

	public virtual void Look(CBaseMechCharacter hOwner,Quaternion qLookDirection){}


	public virtual void Aim(CBaseMechCharacter hOwner){}


	public virtual void Fire(CBaseMechCharacter hOwner){}


	public virtual void Move(CBaseMechCharacter hOwner , Vector3 vMoveDirectioin){}


	public virtual void Crouch(CBaseMechCharacter hOwner){}


	public virtual void Jump(CBaseMechCharacter hOwner){}


	public virtual void Sprint(CBaseMechCharacter hOwner){}


	public virtual void Interact(CBaseMechCharacter hOwner)
	{
		TriggerInteractArea (hOwner);
	}


	public virtual void EquipWeapon(CBaseMechCharacter hOwner){}

	void OnDrawGizmos()
	{
		Gizmos.DrawWireSphere (vSpherePosition, m_fInteractSphereRadius);
	}
}
