﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CMainMechController : CBaseMechController {

	public float m_fLookSpeed;



	public override void Look (CBaseMechCharacter hOwner, Quaternion qLookDirection)
	{
//		print ("LOOK");
//		base.Look (hOwner, fMouseX, fMouseY);qlook
//		hOwner.transform.Rotate(new Vector3(0,vLookDirection.y  ,0));
		hOwner.transform.rotation = Quaternion.Lerp(hOwner.transform.rotation , qLookDirection,m_fLookSpeed);

	}

	public override void Move (CBaseMechCharacter hOwner , Vector3 vMoveDirection)
	{
//		base.Move (hOwner);
//		hOwner.m_fMoveSpeed = hOwner.m_hAttribute.m_fRunSpeed;
		hOwner.transform.Translate(vMoveDirection * Time.deltaTime * hOwner.m_fMoveSpeed);
		hOwner.m_hAnimator.SetFloat ("VerticalButton", vMoveDirection.z);
		hOwner.m_hAnimator.SetFloat ("HorizontalButton", vMoveDirection.x);
	}

	public override void Sprint (CBaseMechCharacter hOwner)
	{
//		base.Sprint (hOwner);
		hOwner.m_fMoveSpeed = hOwner.m_hAttribute.m_fSprintSpeed;
	}

	public override void Crouch (CBaseMechCharacter hOwner)
	{
		hOwner.m_fMoveSpeed = hOwner.m_hAttribute.m_fCrouchSpeed;
	}


}
