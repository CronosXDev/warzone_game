﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MECH_ATTRIBUTE {


	public float m_fHealth;
	public float m_fRunSpeed;
	public float m_fSprintSpeed;
	public float m_fCrouchSpeed;
	public float m_fJumpForce;
}
