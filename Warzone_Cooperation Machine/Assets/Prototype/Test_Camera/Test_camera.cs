﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test_camera : MonoBehaviour {

	public GameObject hMain;
	public float fLerpTime;
	public Transform hLook;
	public float fDistance;
	public Camera hcam;
	public float fMinYAngle;
	public float fMaxYAngle;
	public float fMouseSpeed;

	Vector3 vDistance;
	Quaternion qRotation;
	float fMouseX;
	float fMouseY;

	// Use this for initialization
	void Update () {
		if (Input.GetKey (KeyCode.W)) {
			Quaternion qCamDirection = Quaternion.Euler (0,hcam.transform.rotation.eulerAngles.y, 0);
			hMain.transform.rotation = Quaternion.Lerp (hMain.transform.rotation, qCamDirection, fLerpTime);
		}
	}
	
	// Update is called once per frame
	void LateUpdate () {

		hcam.transform.position = hLook.position + qRotation * vDistance;
		hcam.transform.LookAt (hLook);

		fMouseX += Input.GetAxis ("Mouse X") * fMouseSpeed;
		fMouseY += Input.GetAxis ("Mouse Y") * fMouseSpeed;



		vDistance = new Vector3 (0, 0, -fDistance);
		qRotation.eulerAngles = new Vector3 (fMouseY*-1, fMouseX, 0);



	}
}
